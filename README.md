# Robin Andrei Serrano

I am currently a 4th year Computer Science student from Holy Angel University.

My current goal is to become a Full-Stack Web Developer, but my current skills are for Front-End. I have learned programming languages such as Python, Java, JavaScript, Dart and Flutter. I chose Software Development as a career because I enjoy continuously learning how to improve in coding skills to create a wide array of applications that can be very helpful to others.

# Contact Information

LinkedIn: [https://www.linkedin.com/in/robin-andrei-serrano-5b2864166/](https://www.linkedin.com/in/robin-andrei-serrano-5b2864166/)

# Work Experience

## Flutter Developer Trainee

FFUF Manila, July 2021 to August 2021

- I was very fortunate to be included in the 20 selected flutter trainees.
- The training consisted of JS OOP, Dart & Flutter, Best Coding Practices, FFUF Internal Tools

# Skills

## Technical Skills

- Mobile Application Development (Flutter)
- Python
- Java
- JavaScript | React
- HTML & CSS
- MySQL | PostgreSQL

## Soft Skills

- Interpersonal Skills
- Problem-Solving
- Teamwork
- Adaptability
- Work Ethic
- Time Management

# Education

## BS Computer Science

Holy Angel University, S.Y 2018-Present